const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs')
//init app
const app = express();

//pug view
app.set('views', path.join(__dirname, 'views')); 
app.set('view engine', 'pug');
//public folder 
app.use(express.static('public'));

// Set Storage Engine 
const storage= multer.diskStorage({
    destination: './public/uploads/',
    filename: function(req,file, cb){
       cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));

    }
});

//init upload
const upload = multer({
    storage: storage,
    limits: {filesize: 1000000}, 
    fileFilter: function(req,file, cb) {
    checkFileType(file, cb)
    }
}).single("myImage")

function checkFileType(file, cb) {
    //allowed ext
    const filetypes = /jpeg|jpg|png|gif/;
    //Check ext 
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
    const mimetype = filetypes.test(file.mimetype)
    if(mimetype && extname) {
        return cb(null, true)
    }else {
        cb('Error: Images Only')
    }
}

app.get('/', (req, res) => {
    let paths = './public/uploads'; 
    fs.readdir(paths, function(err, items) {   
        res.render('index', { pictureNames: items })
    });
})

app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
        if (err) {
            res.render('index' , {
                msg: err
            }) 
        }else {
            if(req.file == undefined) {
                res.render('index' , {
                    msg: 'Error no file selected'
                }) 
            }else {
               
                res.render('second', {               
                msg: 'File Uploaded!',
                file:`uploads/${req.file.filename}`
                })
            }
        }
    })
})

const port = 3000;

app.listen(port, () => console.log(`Server started on port ${port}...`))